class SensorDatum < ApplicationRecord
  require 'csv'

  def self.import(file)
    # CSV.foreach(file.path, headers: true, encoding:'iso-8859-1:utf-8') do |row|
		# 	row[0] = id_c.to_i
		# 	product_hash = row.to_hash
    #   		product = find_or_create_by!(id: id_c)
    #   		product.update_attributes(product_hash)
		# end
      CSV.foreach(file.path, headers:true) do |row|
        # SensorDatum.create! row.to_hash

        item_hash = row.to_hash

        if item_hash['id'].to_s != ""
          
          if item_hash['id'].to_i == 1
            count_id = 1
            item_hash.each do |index,item|
              if index.to_s != "id"
                hash_header = Hash.new
                hash_header["id"] = count_id
                hash_header["description"] = index.to_s
                hash_header["data_sensor"] = item.to_s

                dataheader_hash = hash_header.to_hash
                dh_n = DataHeader.find_or_create_by!(id: hash_header['id'])
                dh_n.update_attributes(dataheader_hash)
                count_id += 1
              end
            end
          end

          product_hash = row.to_hash
          product = SensorDatum.find_or_create_by!(id: item_hash['id'])
          product.update_attributes(product_hash)
        end
      end
  end
end
