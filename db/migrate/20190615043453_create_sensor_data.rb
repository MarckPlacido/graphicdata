class CreateSensorData < ActiveRecord::Migration[5.2]
  def change
    create_table :sensor_data do |t|
      t.decimal :sensor_1
      t.decimal :sensor_2
      t.decimal :sensor_3
      t.decimal :sensor_4
      t.decimal :sensor_5
      t.decimal :sensor_6
      t.decimal :sensor_7
      t.decimal :sensor_8
      t.decimal :sensor_9
      t.decimal :sensor_10
      t.decimal :sensor_11
      t.decimal :sensor_12
      t.decimal :sensor_13
      t.decimal :sensor_14
      t.decimal :sensor_15
      t.decimal :sensor_16
      t.decimal :sensor_17
      t.decimal :sensor_18
      t.decimal :sensor_19
      t.decimal :sensor_20
      t.decimal :sensor_21
      t.decimal :sensor_22
      t.decimal :sensor_23
      t.decimal :sensor_24
      t.decimal :sensor_25
      t.decimal :sensor_26
      t.decimal :sensor_27
      t.decimal :sensor_28
      t.decimal :sensor_29
      t.decimal :sensor_30
      t.decimal :sensor_31
      t.decimal :sensor_32
      t.decimal :sensor_33
      t.decimal :sensor_34
      t.decimal :sensor_35
      t.decimal :sensor_36
      t.decimal :sensor_37
      t.decimal :sensor_38
      t.decimal :sensor_39
      t.decimal :sensor_40
      t.decimal :sensor_41
      t.decimal :sensor_42
      t.decimal :sensor_43
      t.decimal :sensor_44
      t.decimal :sensor_45
      t.decimal :sensor_46
      t.decimal :sensor_47
      t.decimal :sensor_48
      t.decimal :sensor_49
      t.decimal :sensor_50
      t.decimal :sensor_51
      t.decimal :sensor_52
      t.decimal :sensor_53
      t.decimal :sensor_54
      t.decimal :sensor_55
      t.decimal :sensor_56
      t.decimal :sensor_57
      t.decimal :sensor_58
      t.decimal :sensor_59
      t.decimal :sensor_60
      t.decimal :sensor_61
      t.decimal :sensor_62
      t.decimal :sensor_63
      t.decimal :sensor_64
      t.decimal :sensor_65
      t.decimal :sensor_66
      t.decimal :sensor_67
      t.decimal :sensor_68
      t.decimal :sensor_69    

      t.timestamps
    end
  end
end
