class CreateOvenTimes < ActiveRecord::Migration[5.2]
  def change
    create_table :oven_times do |t|
      t.string :horarios

      t.timestamps
    end
  end
end
