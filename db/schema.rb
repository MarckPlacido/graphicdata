# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_06_24_201751) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "data_headers", force: :cascade do |t|
    t.string "description"
    t.decimal "data_sensor"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "oven_times", force: :cascade do |t|
    t.string "horarios"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sensor_data", force: :cascade do |t|
    t.decimal "sensor_1"
    t.decimal "sensor_2"
    t.decimal "sensor_3"
    t.decimal "sensor_4"
    t.decimal "sensor_5"
    t.decimal "sensor_6"
    t.decimal "sensor_7"
    t.decimal "sensor_8"
    t.decimal "sensor_9"
    t.decimal "sensor_10"
    t.decimal "sensor_11"
    t.decimal "sensor_12"
    t.decimal "sensor_13"
    t.decimal "sensor_14"
    t.decimal "sensor_15"
    t.decimal "sensor_16"
    t.decimal "sensor_17"
    t.decimal "sensor_18"
    t.decimal "sensor_19"
    t.decimal "sensor_20"
    t.decimal "sensor_21"
    t.decimal "sensor_22"
    t.decimal "sensor_23"
    t.decimal "sensor_24"
    t.decimal "sensor_25"
    t.decimal "sensor_26"
    t.decimal "sensor_27"
    t.decimal "sensor_28"
    t.decimal "sensor_29"
    t.decimal "sensor_30"
    t.decimal "sensor_31"
    t.decimal "sensor_32"
    t.decimal "sensor_33"
    t.decimal "sensor_34"
    t.decimal "sensor_35"
    t.decimal "sensor_36"
    t.decimal "sensor_37"
    t.decimal "sensor_38"
    t.decimal "sensor_39"
    t.decimal "sensor_40"
    t.decimal "sensor_41"
    t.decimal "sensor_42"
    t.decimal "sensor_43"
    t.decimal "sensor_44"
    t.decimal "sensor_45"
    t.decimal "sensor_46"
    t.decimal "sensor_47"
    t.decimal "sensor_48"
    t.decimal "sensor_49"
    t.decimal "sensor_50"
    t.decimal "sensor_51"
    t.decimal "sensor_52"
    t.decimal "sensor_53"
    t.decimal "sensor_54"
    t.decimal "sensor_55"
    t.decimal "sensor_56"
    t.decimal "sensor_57"
    t.decimal "sensor_58"
    t.decimal "sensor_59"
    t.decimal "sensor_60"
    t.decimal "sensor_61"
    t.decimal "sensor_62"
    t.decimal "sensor_63"
    t.decimal "sensor_64"
    t.decimal "sensor_65"
    t.decimal "sensor_66"
    t.decimal "sensor_67"
    t.decimal "sensor_68"
    t.decimal "sensor_69"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "user_name"
    t.string "email"
    t.string "password_digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "type_user"
    t.index ["email"], name: "index_users_on_email", unique: true
  end

end
