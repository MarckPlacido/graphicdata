class SessionsController < ApplicationController
  def new
  end

  def create
    # user = User.find_by_email(params[:email])
    # if user && user.authenticate(params[:password])
    #   session[:user_id] = user.id
    #   redirect_to root_url, notice: "Logged in!"
    # else
    #   flash.now[:alert] = "Email or password is invalid"
    #   render "new"
    # end
    user = User.find_by_email(params[:email])
   # If the user exists AND the password entered is correct.
   if user && user.authenticate(params[:password])
     # Save the user id inside the browser cookie. This is how we keep the user
     # logged in when they navigate around our website.
     session[:user_id] = user.id
     redirect_to home_index_path
   else
   # If user's login doesn't work, send them back to the login form.
     redirect_to login_path, notice: "error, email o password incorrecto"
   end
  end

  def destroy
    session[:user_id] = nil
    #redirect_to root_url, notice: "Logged out!"
    redirect_to login_path, notice: "Logged out!"
  end
end
