require 'test_helper'

class SensorDataControllerTest < ActionDispatch::IntegrationTest
  setup do
    @sensor_datum = sensor_data(:one)
  end

  test "should get index" do
    get sensor_data_url
    assert_response :success
  end

  test "should get new" do
    get new_sensor_datum_url
    assert_response :success
  end

  test "should create sensor_datum" do
    assert_difference('SensorDatum.count') do
      post sensor_data_url, params: { sensor_datum: { decimal: @sensor_datum.decimal, sensor_1: @sensor_datum.sensor_1, sensor_10: @sensor_datum.sensor_10, sensor_100: @sensor_datum.sensor_100, sensor_11: @sensor_datum.sensor_11, sensor_12: @sensor_datum.sensor_12, sensor_13: @sensor_datum.sensor_13, sensor_14: @sensor_datum.sensor_14, sensor_15: @sensor_datum.sensor_15, sensor_16: @sensor_datum.sensor_16, sensor_17: @sensor_datum.sensor_17, sensor_18: @sensor_datum.sensor_18, sensor_19: @sensor_datum.sensor_19, sensor_2: @sensor_datum.sensor_2, sensor_20: @sensor_datum.sensor_20, sensor_21: @sensor_datum.sensor_21, sensor_22: @sensor_datum.sensor_22, sensor_23: @sensor_datum.sensor_23, sensor_24: @sensor_datum.sensor_24, sensor_25: @sensor_datum.sensor_25, sensor_26: @sensor_datum.sensor_26, sensor_27: @sensor_datum.sensor_27, sensor_28: @sensor_datum.sensor_28, sensor_29: @sensor_datum.sensor_29, sensor_3: @sensor_datum.sensor_3, sensor_30: @sensor_datum.sensor_30, sensor_31: @sensor_datum.sensor_31, sensor_32: @sensor_datum.sensor_32, sensor_33: @sensor_datum.sensor_33, sensor_34: @sensor_datum.sensor_34, sensor_35: @sensor_datum.sensor_35, sensor_36: @sensor_datum.sensor_36, sensor_37: @sensor_datum.sensor_37, sensor_38: @sensor_datum.sensor_38, sensor_39: @sensor_datum.sensor_39, sensor_4: @sensor_datum.sensor_4, sensor_40: @sensor_datum.sensor_40, sensor_41: @sensor_datum.sensor_41, sensor_42: @sensor_datum.sensor_42, sensor_43: @sensor_datum.sensor_43, sensor_44: @sensor_datum.sensor_44, sensor_45: @sensor_datum.sensor_45, sensor_46: @sensor_datum.sensor_46, sensor_47: @sensor_datum.sensor_47, sensor_48: @sensor_datum.sensor_48, sensor_49: @sensor_datum.sensor_49, sensor_5: @sensor_datum.sensor_5, sensor_50: @sensor_datum.sensor_50, sensor_51: @sensor_datum.sensor_51, sensor_52: @sensor_datum.sensor_52, sensor_53: @sensor_datum.sensor_53, sensor_54: @sensor_datum.sensor_54, sensor_55: @sensor_datum.sensor_55, sensor_56: @sensor_datum.sensor_56, sensor_57: @sensor_datum.sensor_57, sensor_58: @sensor_datum.sensor_58, sensor_59: @sensor_datum.sensor_59, sensor_6: @sensor_datum.sensor_6, sensor_60: @sensor_datum.sensor_60, sensor_61: @sensor_datum.sensor_61, sensor_62: @sensor_datum.sensor_62, sensor_63: @sensor_datum.sensor_63, sensor_64: @sensor_datum.sensor_64, sensor_65: @sensor_datum.sensor_65, sensor_66: @sensor_datum.sensor_66, sensor_67: @sensor_datum.sensor_67, sensor_68: @sensor_datum.sensor_68, sensor_69: @sensor_datum.sensor_69, sensor_7: @sensor_datum.sensor_7, sensor_70: @sensor_datum.sensor_70, sensor_71: @sensor_datum.sensor_71, sensor_72: @sensor_datum.sensor_72, sensor_73: @sensor_datum.sensor_73, sensor_74: @sensor_datum.sensor_74, sensor_75: @sensor_datum.sensor_75, sensor_76: @sensor_datum.sensor_76, sensor_77: @sensor_datum.sensor_77, sensor_78: @sensor_datum.sensor_78, sensor_79: @sensor_datum.sensor_79, sensor_8: @sensor_datum.sensor_8, sensor_80: @sensor_datum.sensor_80, sensor_81: @sensor_datum.sensor_81, sensor_82: @sensor_datum.sensor_82, sensor_83: @sensor_datum.sensor_83, sensor_84: @sensor_datum.sensor_84, sensor_85: @sensor_datum.sensor_85, sensor_86: @sensor_datum.sensor_86, sensor_87: @sensor_datum.sensor_87, sensor_88: @sensor_datum.sensor_88, sensor_89: @sensor_datum.sensor_89, sensor_9: @sensor_datum.sensor_9, sensor_90: @sensor_datum.sensor_90, sensor_91: @sensor_datum.sensor_91, sensor_92: @sensor_datum.sensor_92, sensor_93: @sensor_datum.sensor_93, sensor_94: @sensor_datum.sensor_94, sensor_95: @sensor_datum.sensor_95, sensor_96: @sensor_datum.sensor_96, sensor_97: @sensor_datum.sensor_97, sensor_98: @sensor_datum.sensor_98, sensor_99: @sensor_datum.sensor_99 } }
    end

    assert_redirected_to sensor_datum_url(SensorDatum.last)
  end

  test "should show sensor_datum" do
    get sensor_datum_url(@sensor_datum)
    assert_response :success
  end

  test "should get edit" do
    get edit_sensor_datum_url(@sensor_datum)
    assert_response :success
  end

  test "should update sensor_datum" do
    patch sensor_datum_url(@sensor_datum), params: { sensor_datum: { decimal: @sensor_datum.decimal, sensor_1: @sensor_datum.sensor_1, sensor_10: @sensor_datum.sensor_10, sensor_100: @sensor_datum.sensor_100, sensor_11: @sensor_datum.sensor_11, sensor_12: @sensor_datum.sensor_12, sensor_13: @sensor_datum.sensor_13, sensor_14: @sensor_datum.sensor_14, sensor_15: @sensor_datum.sensor_15, sensor_16: @sensor_datum.sensor_16, sensor_17: @sensor_datum.sensor_17, sensor_18: @sensor_datum.sensor_18, sensor_19: @sensor_datum.sensor_19, sensor_2: @sensor_datum.sensor_2, sensor_20: @sensor_datum.sensor_20, sensor_21: @sensor_datum.sensor_21, sensor_22: @sensor_datum.sensor_22, sensor_23: @sensor_datum.sensor_23, sensor_24: @sensor_datum.sensor_24, sensor_25: @sensor_datum.sensor_25, sensor_26: @sensor_datum.sensor_26, sensor_27: @sensor_datum.sensor_27, sensor_28: @sensor_datum.sensor_28, sensor_29: @sensor_datum.sensor_29, sensor_3: @sensor_datum.sensor_3, sensor_30: @sensor_datum.sensor_30, sensor_31: @sensor_datum.sensor_31, sensor_32: @sensor_datum.sensor_32, sensor_33: @sensor_datum.sensor_33, sensor_34: @sensor_datum.sensor_34, sensor_35: @sensor_datum.sensor_35, sensor_36: @sensor_datum.sensor_36, sensor_37: @sensor_datum.sensor_37, sensor_38: @sensor_datum.sensor_38, sensor_39: @sensor_datum.sensor_39, sensor_4: @sensor_datum.sensor_4, sensor_40: @sensor_datum.sensor_40, sensor_41: @sensor_datum.sensor_41, sensor_42: @sensor_datum.sensor_42, sensor_43: @sensor_datum.sensor_43, sensor_44: @sensor_datum.sensor_44, sensor_45: @sensor_datum.sensor_45, sensor_46: @sensor_datum.sensor_46, sensor_47: @sensor_datum.sensor_47, sensor_48: @sensor_datum.sensor_48, sensor_49: @sensor_datum.sensor_49, sensor_5: @sensor_datum.sensor_5, sensor_50: @sensor_datum.sensor_50, sensor_51: @sensor_datum.sensor_51, sensor_52: @sensor_datum.sensor_52, sensor_53: @sensor_datum.sensor_53, sensor_54: @sensor_datum.sensor_54, sensor_55: @sensor_datum.sensor_55, sensor_56: @sensor_datum.sensor_56, sensor_57: @sensor_datum.sensor_57, sensor_58: @sensor_datum.sensor_58, sensor_59: @sensor_datum.sensor_59, sensor_6: @sensor_datum.sensor_6, sensor_60: @sensor_datum.sensor_60, sensor_61: @sensor_datum.sensor_61, sensor_62: @sensor_datum.sensor_62, sensor_63: @sensor_datum.sensor_63, sensor_64: @sensor_datum.sensor_64, sensor_65: @sensor_datum.sensor_65, sensor_66: @sensor_datum.sensor_66, sensor_67: @sensor_datum.sensor_67, sensor_68: @sensor_datum.sensor_68, sensor_69: @sensor_datum.sensor_69, sensor_7: @sensor_datum.sensor_7, sensor_70: @sensor_datum.sensor_70, sensor_71: @sensor_datum.sensor_71, sensor_72: @sensor_datum.sensor_72, sensor_73: @sensor_datum.sensor_73, sensor_74: @sensor_datum.sensor_74, sensor_75: @sensor_datum.sensor_75, sensor_76: @sensor_datum.sensor_76, sensor_77: @sensor_datum.sensor_77, sensor_78: @sensor_datum.sensor_78, sensor_79: @sensor_datum.sensor_79, sensor_8: @sensor_datum.sensor_8, sensor_80: @sensor_datum.sensor_80, sensor_81: @sensor_datum.sensor_81, sensor_82: @sensor_datum.sensor_82, sensor_83: @sensor_datum.sensor_83, sensor_84: @sensor_datum.sensor_84, sensor_85: @sensor_datum.sensor_85, sensor_86: @sensor_datum.sensor_86, sensor_87: @sensor_datum.sensor_87, sensor_88: @sensor_datum.sensor_88, sensor_89: @sensor_datum.sensor_89, sensor_9: @sensor_datum.sensor_9, sensor_90: @sensor_datum.sensor_90, sensor_91: @sensor_datum.sensor_91, sensor_92: @sensor_datum.sensor_92, sensor_93: @sensor_datum.sensor_93, sensor_94: @sensor_datum.sensor_94, sensor_95: @sensor_datum.sensor_95, sensor_96: @sensor_datum.sensor_96, sensor_97: @sensor_datum.sensor_97, sensor_98: @sensor_datum.sensor_98, sensor_99: @sensor_datum.sensor_99 } }
    assert_redirected_to sensor_datum_url(@sensor_datum)
  end

  test "should destroy sensor_datum" do
    assert_difference('SensorDatum.count', -1) do
      delete sensor_datum_url(@sensor_datum)
    end

    assert_redirected_to sensor_data_url
  end
end
