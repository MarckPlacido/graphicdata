require "application_system_test_case"

class SensorDataTest < ApplicationSystemTestCase
  setup do
    @sensor_datum = sensor_data(:one)
  end

  test "visiting the index" do
    visit sensor_data_url
    assert_selector "h1", text: "Sensor Data"
  end

  test "creating a Sensor datum" do
    visit sensor_data_url
    click_on "New Sensor Datum"

    fill_in "Decimal", with: @sensor_datum.decimal
    fill_in "Sensor 1", with: @sensor_datum.sensor_1
    fill_in "Sensor 10", with: @sensor_datum.sensor_10
    fill_in "Sensor 100", with: @sensor_datum.sensor_100
    fill_in "Sensor 11", with: @sensor_datum.sensor_11
    fill_in "Sensor 12", with: @sensor_datum.sensor_12
    fill_in "Sensor 13", with: @sensor_datum.sensor_13
    fill_in "Sensor 14", with: @sensor_datum.sensor_14
    fill_in "Sensor 15", with: @sensor_datum.sensor_15
    fill_in "Sensor 16", with: @sensor_datum.sensor_16
    fill_in "Sensor 17", with: @sensor_datum.sensor_17
    fill_in "Sensor 18", with: @sensor_datum.sensor_18
    fill_in "Sensor 19", with: @sensor_datum.sensor_19
    fill_in "Sensor 2", with: @sensor_datum.sensor_2
    fill_in "Sensor 20", with: @sensor_datum.sensor_20
    fill_in "Sensor 21", with: @sensor_datum.sensor_21
    fill_in "Sensor 22", with: @sensor_datum.sensor_22
    fill_in "Sensor 23", with: @sensor_datum.sensor_23
    fill_in "Sensor 24", with: @sensor_datum.sensor_24
    fill_in "Sensor 25", with: @sensor_datum.sensor_25
    fill_in "Sensor 26", with: @sensor_datum.sensor_26
    fill_in "Sensor 27", with: @sensor_datum.sensor_27
    fill_in "Sensor 28", with: @sensor_datum.sensor_28
    fill_in "Sensor 29", with: @sensor_datum.sensor_29
    fill_in "Sensor 3", with: @sensor_datum.sensor_3
    fill_in "Sensor 30", with: @sensor_datum.sensor_30
    fill_in "Sensor 31", with: @sensor_datum.sensor_31
    fill_in "Sensor 32", with: @sensor_datum.sensor_32
    fill_in "Sensor 33", with: @sensor_datum.sensor_33
    fill_in "Sensor 34", with: @sensor_datum.sensor_34
    fill_in "Sensor 35", with: @sensor_datum.sensor_35
    fill_in "Sensor 36", with: @sensor_datum.sensor_36
    fill_in "Sensor 37", with: @sensor_datum.sensor_37
    fill_in "Sensor 38", with: @sensor_datum.sensor_38
    fill_in "Sensor 39", with: @sensor_datum.sensor_39
    fill_in "Sensor 4", with: @sensor_datum.sensor_4
    fill_in "Sensor 40", with: @sensor_datum.sensor_40
    fill_in "Sensor 41", with: @sensor_datum.sensor_41
    fill_in "Sensor 42", with: @sensor_datum.sensor_42
    fill_in "Sensor 43", with: @sensor_datum.sensor_43
    fill_in "Sensor 44", with: @sensor_datum.sensor_44
    fill_in "Sensor 45", with: @sensor_datum.sensor_45
    fill_in "Sensor 46", with: @sensor_datum.sensor_46
    fill_in "Sensor 47", with: @sensor_datum.sensor_47
    fill_in "Sensor 48", with: @sensor_datum.sensor_48
    fill_in "Sensor 49", with: @sensor_datum.sensor_49
    fill_in "Sensor 5", with: @sensor_datum.sensor_5
    fill_in "Sensor 50", with: @sensor_datum.sensor_50
    fill_in "Sensor 51", with: @sensor_datum.sensor_51
    fill_in "Sensor 52", with: @sensor_datum.sensor_52
    fill_in "Sensor 53", with: @sensor_datum.sensor_53
    fill_in "Sensor 54", with: @sensor_datum.sensor_54
    fill_in "Sensor 55", with: @sensor_datum.sensor_55
    fill_in "Sensor 56", with: @sensor_datum.sensor_56
    fill_in "Sensor 57", with: @sensor_datum.sensor_57
    fill_in "Sensor 58", with: @sensor_datum.sensor_58
    fill_in "Sensor 59", with: @sensor_datum.sensor_59
    fill_in "Sensor 6", with: @sensor_datum.sensor_6
    fill_in "Sensor 60", with: @sensor_datum.sensor_60
    fill_in "Sensor 61", with: @sensor_datum.sensor_61
    fill_in "Sensor 62", with: @sensor_datum.sensor_62
    fill_in "Sensor 63", with: @sensor_datum.sensor_63
    fill_in "Sensor 64", with: @sensor_datum.sensor_64
    fill_in "Sensor 65", with: @sensor_datum.sensor_65
    fill_in "Sensor 66", with: @sensor_datum.sensor_66
    fill_in "Sensor 67", with: @sensor_datum.sensor_67
    fill_in "Sensor 68", with: @sensor_datum.sensor_68
    fill_in "Sensor 69", with: @sensor_datum.sensor_69
    fill_in "Sensor 7", with: @sensor_datum.sensor_7
    fill_in "Sensor 70", with: @sensor_datum.sensor_70
    fill_in "Sensor 71", with: @sensor_datum.sensor_71
    fill_in "Sensor 72", with: @sensor_datum.sensor_72
    fill_in "Sensor 73", with: @sensor_datum.sensor_73
    fill_in "Sensor 74", with: @sensor_datum.sensor_74
    fill_in "Sensor 75", with: @sensor_datum.sensor_75
    fill_in "Sensor 76", with: @sensor_datum.sensor_76
    fill_in "Sensor 77", with: @sensor_datum.sensor_77
    fill_in "Sensor 78", with: @sensor_datum.sensor_78
    fill_in "Sensor 79", with: @sensor_datum.sensor_79
    fill_in "Sensor 8", with: @sensor_datum.sensor_8
    fill_in "Sensor 80", with: @sensor_datum.sensor_80
    fill_in "Sensor 81", with: @sensor_datum.sensor_81
    fill_in "Sensor 82", with: @sensor_datum.sensor_82
    fill_in "Sensor 83", with: @sensor_datum.sensor_83
    fill_in "Sensor 84", with: @sensor_datum.sensor_84
    fill_in "Sensor 85", with: @sensor_datum.sensor_85
    fill_in "Sensor 86", with: @sensor_datum.sensor_86
    fill_in "Sensor 87", with: @sensor_datum.sensor_87
    fill_in "Sensor 88", with: @sensor_datum.sensor_88
    fill_in "Sensor 89", with: @sensor_datum.sensor_89
    fill_in "Sensor 9", with: @sensor_datum.sensor_9
    fill_in "Sensor 90", with: @sensor_datum.sensor_90
    fill_in "Sensor 91", with: @sensor_datum.sensor_91
    fill_in "Sensor 92", with: @sensor_datum.sensor_92
    fill_in "Sensor 93", with: @sensor_datum.sensor_93
    fill_in "Sensor 94", with: @sensor_datum.sensor_94
    fill_in "Sensor 95", with: @sensor_datum.sensor_95
    fill_in "Sensor 96", with: @sensor_datum.sensor_96
    fill_in "Sensor 97", with: @sensor_datum.sensor_97
    fill_in "Sensor 98", with: @sensor_datum.sensor_98
    fill_in "Sensor 99", with: @sensor_datum.sensor_99
    click_on "Create Sensor datum"

    assert_text "Sensor datum was successfully created"
    click_on "Back"
  end

  test "updating a Sensor datum" do
    visit sensor_data_url
    click_on "Edit", match: :first

    fill_in "Decimal", with: @sensor_datum.decimal
    fill_in "Sensor 1", with: @sensor_datum.sensor_1
    fill_in "Sensor 10", with: @sensor_datum.sensor_10
    fill_in "Sensor 100", with: @sensor_datum.sensor_100
    fill_in "Sensor 11", with: @sensor_datum.sensor_11
    fill_in "Sensor 12", with: @sensor_datum.sensor_12
    fill_in "Sensor 13", with: @sensor_datum.sensor_13
    fill_in "Sensor 14", with: @sensor_datum.sensor_14
    fill_in "Sensor 15", with: @sensor_datum.sensor_15
    fill_in "Sensor 16", with: @sensor_datum.sensor_16
    fill_in "Sensor 17", with: @sensor_datum.sensor_17
    fill_in "Sensor 18", with: @sensor_datum.sensor_18
    fill_in "Sensor 19", with: @sensor_datum.sensor_19
    fill_in "Sensor 2", with: @sensor_datum.sensor_2
    fill_in "Sensor 20", with: @sensor_datum.sensor_20
    fill_in "Sensor 21", with: @sensor_datum.sensor_21
    fill_in "Sensor 22", with: @sensor_datum.sensor_22
    fill_in "Sensor 23", with: @sensor_datum.sensor_23
    fill_in "Sensor 24", with: @sensor_datum.sensor_24
    fill_in "Sensor 25", with: @sensor_datum.sensor_25
    fill_in "Sensor 26", with: @sensor_datum.sensor_26
    fill_in "Sensor 27", with: @sensor_datum.sensor_27
    fill_in "Sensor 28", with: @sensor_datum.sensor_28
    fill_in "Sensor 29", with: @sensor_datum.sensor_29
    fill_in "Sensor 3", with: @sensor_datum.sensor_3
    fill_in "Sensor 30", with: @sensor_datum.sensor_30
    fill_in "Sensor 31", with: @sensor_datum.sensor_31
    fill_in "Sensor 32", with: @sensor_datum.sensor_32
    fill_in "Sensor 33", with: @sensor_datum.sensor_33
    fill_in "Sensor 34", with: @sensor_datum.sensor_34
    fill_in "Sensor 35", with: @sensor_datum.sensor_35
    fill_in "Sensor 36", with: @sensor_datum.sensor_36
    fill_in "Sensor 37", with: @sensor_datum.sensor_37
    fill_in "Sensor 38", with: @sensor_datum.sensor_38
    fill_in "Sensor 39", with: @sensor_datum.sensor_39
    fill_in "Sensor 4", with: @sensor_datum.sensor_4
    fill_in "Sensor 40", with: @sensor_datum.sensor_40
    fill_in "Sensor 41", with: @sensor_datum.sensor_41
    fill_in "Sensor 42", with: @sensor_datum.sensor_42
    fill_in "Sensor 43", with: @sensor_datum.sensor_43
    fill_in "Sensor 44", with: @sensor_datum.sensor_44
    fill_in "Sensor 45", with: @sensor_datum.sensor_45
    fill_in "Sensor 46", with: @sensor_datum.sensor_46
    fill_in "Sensor 47", with: @sensor_datum.sensor_47
    fill_in "Sensor 48", with: @sensor_datum.sensor_48
    fill_in "Sensor 49", with: @sensor_datum.sensor_49
    fill_in "Sensor 5", with: @sensor_datum.sensor_5
    fill_in "Sensor 50", with: @sensor_datum.sensor_50
    fill_in "Sensor 51", with: @sensor_datum.sensor_51
    fill_in "Sensor 52", with: @sensor_datum.sensor_52
    fill_in "Sensor 53", with: @sensor_datum.sensor_53
    fill_in "Sensor 54", with: @sensor_datum.sensor_54
    fill_in "Sensor 55", with: @sensor_datum.sensor_55
    fill_in "Sensor 56", with: @sensor_datum.sensor_56
    fill_in "Sensor 57", with: @sensor_datum.sensor_57
    fill_in "Sensor 58", with: @sensor_datum.sensor_58
    fill_in "Sensor 59", with: @sensor_datum.sensor_59
    fill_in "Sensor 6", with: @sensor_datum.sensor_6
    fill_in "Sensor 60", with: @sensor_datum.sensor_60
    fill_in "Sensor 61", with: @sensor_datum.sensor_61
    fill_in "Sensor 62", with: @sensor_datum.sensor_62
    fill_in "Sensor 63", with: @sensor_datum.sensor_63
    fill_in "Sensor 64", with: @sensor_datum.sensor_64
    fill_in "Sensor 65", with: @sensor_datum.sensor_65
    fill_in "Sensor 66", with: @sensor_datum.sensor_66
    fill_in "Sensor 67", with: @sensor_datum.sensor_67
    fill_in "Sensor 68", with: @sensor_datum.sensor_68
    fill_in "Sensor 69", with: @sensor_datum.sensor_69
    fill_in "Sensor 7", with: @sensor_datum.sensor_7
    fill_in "Sensor 70", with: @sensor_datum.sensor_70
    fill_in "Sensor 71", with: @sensor_datum.sensor_71
    fill_in "Sensor 72", with: @sensor_datum.sensor_72
    fill_in "Sensor 73", with: @sensor_datum.sensor_73
    fill_in "Sensor 74", with: @sensor_datum.sensor_74
    fill_in "Sensor 75", with: @sensor_datum.sensor_75
    fill_in "Sensor 76", with: @sensor_datum.sensor_76
    fill_in "Sensor 77", with: @sensor_datum.sensor_77
    fill_in "Sensor 78", with: @sensor_datum.sensor_78
    fill_in "Sensor 79", with: @sensor_datum.sensor_79
    fill_in "Sensor 8", with: @sensor_datum.sensor_8
    fill_in "Sensor 80", with: @sensor_datum.sensor_80
    fill_in "Sensor 81", with: @sensor_datum.sensor_81
    fill_in "Sensor 82", with: @sensor_datum.sensor_82
    fill_in "Sensor 83", with: @sensor_datum.sensor_83
    fill_in "Sensor 84", with: @sensor_datum.sensor_84
    fill_in "Sensor 85", with: @sensor_datum.sensor_85
    fill_in "Sensor 86", with: @sensor_datum.sensor_86
    fill_in "Sensor 87", with: @sensor_datum.sensor_87
    fill_in "Sensor 88", with: @sensor_datum.sensor_88
    fill_in "Sensor 89", with: @sensor_datum.sensor_89
    fill_in "Sensor 9", with: @sensor_datum.sensor_9
    fill_in "Sensor 90", with: @sensor_datum.sensor_90
    fill_in "Sensor 91", with: @sensor_datum.sensor_91
    fill_in "Sensor 92", with: @sensor_datum.sensor_92
    fill_in "Sensor 93", with: @sensor_datum.sensor_93
    fill_in "Sensor 94", with: @sensor_datum.sensor_94
    fill_in "Sensor 95", with: @sensor_datum.sensor_95
    fill_in "Sensor 96", with: @sensor_datum.sensor_96
    fill_in "Sensor 97", with: @sensor_datum.sensor_97
    fill_in "Sensor 98", with: @sensor_datum.sensor_98
    fill_in "Sensor 99", with: @sensor_datum.sensor_99
    click_on "Update Sensor datum"

    assert_text "Sensor datum was successfully updated"
    click_on "Back"
  end

  test "destroying a Sensor datum" do
    visit sensor_data_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Sensor datum was successfully destroyed"
  end
end
