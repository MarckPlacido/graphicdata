class CreateDataHeaders < ActiveRecord::Migration[5.2]
  def change
    create_table :data_headers do |t|
      t.string :description
      t.decimal :data_sensor

      t.timestamps
    end
  end
end
