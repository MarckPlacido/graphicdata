class HomeController < ApplicationController
   before_action :authorize
  def index
    @value_max_min = DataHeader.all
        @value_min = @value_max_min.minimum("data_sensor")
        @value_max = @value_max_min.maximum("data_sensor")

    @date_ini = DateTime.now.strftime("%d/%m/%y %H:%M:%S")
    data_inti_sensor = []

    if params[:sensor_1]
      data_inti_sensor.push(params[:sensor_1])
    end

    if params[:sensor_2]
      data_inti_sensor.push(params[:sensor_2])
    end

    if params[:sensor_3]
      data_inti_sensor.push(params[:sensor_3])
    end

    if params[:sensor_4]
      data_inti_sensor.push(params[:sensor_4])
    end

    if params['fecha_grafica']
  		@date_ini = params['fecha_grafica']
  	end

    indice_fin = "57"
    incremento_cel = 30

    if data_inti_sensor.count == 4

      data_headers = []
      count_h = 0
      data_print_txt = ''
      while data_headers.count <= 3
        headers = DataHeader.select("description").where("data_sensor >= ?", data_inti_sensor[count_h] ).order("data_sensor ASC").first
        if data_headers.include?(headers.description.to_s)
          data_inti_sensor[count_h] = (data_inti_sensor[count_h].to_d + 0.10)
        else
          data_headers.push(headers.description.to_s)
          count_h += 1
        end
      end

      search_data_tbl_1 = SensorDatum.select(data_headers[0].to_s + " as description").order(data_headers[0].to_s + " asc")
      search_data_tbl_2 = SensorDatum.select(data_headers[1].to_s + " as description").order(data_headers[1].to_s + " asc")
      search_data_tbl_3 = SensorDatum.select(data_headers[2].to_s + " as description").order(data_headers[2].to_s + " asc")
      search_data_tbl_4 = SensorDatum.select(data_headers[3].to_s + " as description").order(data_headers[3].to_s + " asc")

      data_comparacion = []

      count_1 = 0
      search_data_tbl_1.each do |tbl|
        if tbl.description.to_s.include?(indice_fin)
          data_comparacion.push(count_1)
          break
        end
        count_1 += 1
      end

      count_2 = 0
      search_data_tbl_2.each do |tbl|
        if tbl.description.to_s.include?(indice_fin)
          data_comparacion.push(count_2)
          break
        end
        count_2 += 1
      end

      count_3 = 0
      search_data_tbl_3.each do |tbl|
        if tbl.description.to_s.include?(indice_fin)
          data_comparacion.push(count_3)
          break
        end
        count_3 += 1
      end

      count_4 = 0
      search_data_tbl_4.each do |tbl|
        if tbl.description.to_s.include?(indice_fin)
          data_comparacion.push(count_4)
          break
        end
        count_4 += 1
      end

      search_time = OvenTime.select("horarios").order("horarios ASC")

      array_time = []
      incremen_time = 0
      array_time_seles_h = @date_ini.split(" ")
      array_time_seles = array_time_seles_h[1].split(":")
      time_seles = array_time_seles[0] + ":" + array_time_seles[1]

      while array_time.count <= (search_data_tbl_1.length - 1)
        if array_time.count != 0
          array_time.push(search_time[incremen_time].horarios)
        else
          if search_time[incremen_time].horarios.include?(time_seles.to_s)
              array_time.push(search_time[incremen_time].horarios)
          end
        end
        incremen_time += 1
      end

      array_seconds = [":00",":01",":02",":03",":04",":05",":06",":07",":08",":09",":10",":11",":12",":13",":14",":15",":16",":17",":18",":19",":20",":21",":22",":23",":24",":25",":26",":27",":28",":29",":30",":31",":32",":33",":34",":35",":36",":37",":38",":39",":40",":41",":42",":43",":44",":45",":46",":47",":48",":49",":50",":51",":52",":53",":54",":55",":56",":57",":58",":59"]

      @dorting_data = data_comparacion.sort {|a, b| a <=> b}
      data_print_txt = "Date;Time;Temp (°C);Temp (°C);Temp (°C);Temp (°C) \n"
      for i in 0..(@dorting_data[3] + incremento_cel )
        item_t = ":"+array_time_seles[2].to_s #array_seconds[rand(array_seconds.length)]
        data_print_txt += (array_time_seles_h[0].to_s + ";" + array_time[i].to_s + item_t.to_s + ";" + search_data_tbl_1[i].description.to_s + ";" + search_data_tbl_2[i].description.to_s + ";" + search_data_tbl_3[i].description.to_s + ";" + search_data_tbl_4[i].description.to_s + "\n")
      end

      send_data(data_print_txt, :filename => 'valores_sensores_' + @date_ini.to_s + '.txt')
    end

  end
end
